package br.com.investimentos.Investimentos.repositories;

import br.com.investimentos.Investimentos.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {
}
