package br.com.investimentos.Investimentos.repositories;

import br.com.investimentos.Investimentos.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer> {
}
