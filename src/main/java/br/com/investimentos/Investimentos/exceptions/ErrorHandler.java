package br.com.investimentos.Investimentos.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.List;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public HashMap<String, String> manipulacaoDeErrosDeValidacao(MethodArgumentNotValidException exception) {
        HashMap<String, String> erros = new HashMap<>();
        BindingResult resultado = exception.getBindingResult();

        //Lista de erros
        List<FieldError> fieldErrorList = resultado.getFieldErrors();

        for (FieldError erro : fieldErrorList) {
            erros.put(erro.getField(), erro.getDefaultMessage());
        }

        return erros;
    }
}
