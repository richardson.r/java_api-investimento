package br.com.investimentos.Investimentos.services;

import br.com.investimentos.Investimentos.DTOs.RendimentoSimulacaoDTO;
import br.com.investimentos.Investimentos.DTOs.SimulacaoDTO;
import br.com.investimentos.Investimentos.models.Investimento;
import br.com.investimentos.Investimentos.models.Simulacao;
import br.com.investimentos.Investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    public Simulacao buscarSimulacaoPeloId(int id) {
        Optional<Simulacao> simulacaoOptional = simulacaoRepository.findById(id);
        if (simulacaoOptional.isPresent())
            return simulacaoOptional.get();
        else {
            throw new RuntimeException("Simulação (" + id + ") não encontrada");
        }
    }

    public void deletarSimulacao(int id) {
        if (simulacaoRepository.existsById(id))
            simulacaoRepository.deleteById(id);
        else
            throw new RuntimeException("Registro não existe");
    }

    public List<SimulacaoDTO> lerTodasSimulacoes() {
        List<SimulacaoDTO> simulacoesDTO = new ArrayList<>();

        for (Simulacao simulacao : simulacaoRepository.findAll()) {
            SimulacaoDTO simulacaoDTO = new SimulacaoDTO();
            simulacaoDTO.setIdInvestimento(simulacao.getInvestimento().getId());
            simulacaoDTO.setEmail(simulacao.getEmail());
            simulacaoDTO.setValorAplicado(simulacao.getValorAplicado());
            simulacaoDTO.setNomeUsuario(simulacao.getNomeUsuario());
            simulacaoDTO.setQuantidadeMeses(simulacao.getQuantidadeMeses());

            simulacoesDTO.add(simulacaoDTO);
        }

        return simulacoesDTO;
    }

    public RendimentoSimulacaoDTO salvarSimulacao(SimulacaoDTO simulacaoDTO) {

        Simulacao simulacao = new Simulacao();
        RendimentoSimulacaoDTO rendimentoSimulacaoDTO = new RendimentoSimulacaoDTO();
        simulacao = simulacaoDTO.converterParaSimulacao();

        Investimento investimento = new Investimento();
        investimento = investimentoService.buscarInvestimentoPeloId(simulacaoDTO.getIdInvestimento());

        simulacao.setInvestimento(investimento);

        simulacao = simulacaoRepository.save(simulacao);

        rendimentoSimulacaoDTO = calcularRendimento(simulacao);

        return rendimentoSimulacaoDTO;
    }

    public RendimentoSimulacaoDTO calcularRendimento(Simulacao simulacao) {
        RendimentoSimulacaoDTO rendimentoSimulacaoDTO = new RendimentoSimulacaoDTO();

        BigDecimal rendimentoAoMes = simulacao.getInvestimento().getRendimentoAoMes();
        BigDecimal qtdMeses = new BigDecimal(simulacao.getQuantidadeMeses());

        BigDecimal montante = (simulacao.getValorAplicado()
                .multiply(rendimentoAoMes.divide(new BigDecimal("100")))
                .multiply(qtdMeses))
                .add(simulacao.getValorAplicado());

        rendimentoSimulacaoDTO.setRendimentoAoMes(rendimentoAoMes.setScale(2, BigDecimal.ROUND_UNNECESSARY));
        rendimentoSimulacaoDTO.setMontante(montante.setScale(2, BigDecimal.ROUND_UNNECESSARY));

        return rendimentoSimulacaoDTO;
    }
}
