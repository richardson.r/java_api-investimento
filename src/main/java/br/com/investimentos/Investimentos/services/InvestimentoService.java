package br.com.investimentos.Investimentos.services;

import br.com.investimentos.Investimentos.models.Investimento;
import br.com.investimentos.Investimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento salvarInvestimento(Investimento investimento) {
        try {
            return investimentoRepository.save(investimento);
        } catch (DataIntegrityViolationException e) {
            throw new RuntimeException("Não foi possível salvar. Investimento já existente.");
        }
    }

    public Investimento buscarInvestimentoPeloId(int id) {
        Optional<Investimento> investimentoOptional = investimentoRepository.findById(id);
        if (investimentoOptional.isPresent())
            return investimentoOptional.get();
        else
            throw new RuntimeException("Investimento não encontrado");
    }

    public Iterable<Investimento> lerTodosInvestimentos() {
        return investimentoRepository.findAll();
    }

    public void deletarInvestimento(int id) {
        if (investimentoRepository.existsById(id))
            investimentoRepository.deleteById(id);
        else
            throw new RuntimeException("Registro não existe");
    }
}
