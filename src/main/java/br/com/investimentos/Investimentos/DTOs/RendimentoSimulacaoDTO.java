package br.com.investimentos.Investimentos.DTOs;

import java.math.BigDecimal;

public class RendimentoSimulacaoDTO {

    private BigDecimal rendimentoAoMes;

    private BigDecimal montante;

    public BigDecimal getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(BigDecimal rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public BigDecimal getMontante() {
        return montante;
    }

    public void setMontante(BigDecimal montante) {
        this.montante = montante;
    }

    public RendimentoSimulacaoDTO() {
    }
}
