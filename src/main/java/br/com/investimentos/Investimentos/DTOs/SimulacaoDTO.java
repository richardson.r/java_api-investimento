package br.com.investimentos.Investimentos.DTOs;

import br.com.investimentos.Investimentos.models.Simulacao;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class SimulacaoDTO {

    private int idInvestimento;

    @NotNull
    @Digits(integer = 12, fraction = 2, message = "Valor aplicado fora do padrão")
    @DecimalMin(value = "0.01", message = "O valor aplicado não pode ser inferior a 0.01")
    private BigDecimal valorAplicado;

    @NotNull
    @DecimalMin(value = "1", message = "A quantidade mínima de meses é 1")
    private int quantidadeMeses;

    @NotBlank
    @NotNull
    private String nomeUsuario;

    @NotBlank
    @NotNull
    private String email;

    public SimulacaoDTO() {
    }

    public int getIdInvestimento() {
        return idInvestimento;
    }

    public void setIdInvestimento(int idInvestimento) {
        this.idInvestimento = idInvestimento;
    }

    public BigDecimal getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(BigDecimal valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public Simulacao converterParaSimulacao() {
        Simulacao simulacao = new Simulacao();
        simulacao.setNomeUsuario(this.nomeUsuario);
        simulacao.setEmail(this.email);
        simulacao.setValorAplicado(this.valorAplicado);
        simulacao.setQuantidadeMeses(this.quantidadeMeses);

        return simulacao;
    }
}
