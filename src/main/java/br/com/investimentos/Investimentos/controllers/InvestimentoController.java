package br.com.investimentos.Investimentos.controllers;

import br.com.investimentos.Investimentos.DTOs.RendimentoSimulacaoDTO;
import br.com.investimentos.Investimentos.DTOs.SimulacaoDTO;
import br.com.investimentos.Investimentos.models.Investimento;
import br.com.investimentos.Investimentos.models.Simulacao;
import br.com.investimentos.Investimentos.services.InvestimentoService;
import br.com.investimentos.Investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<Investimento> lerTodosInvestimentos(){
        return investimentoService.lerTodosInvestimentos();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED) // 201 Created
    public Investimento registrarInvestimento(@RequestBody @Valid Investimento investimento){
        try {
            return investimentoService.salvarInvestimento(investimento);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    @PostMapping("/{idInvestimento}/simulacao")
    public RendimentoSimulacaoDTO simularInvestimento(@RequestBody @Valid SimulacaoDTO simulacaoDTO, @PathVariable(name = "idInvestimento") int idInvestimento){
        simulacaoDTO.setIdInvestimento(idInvestimento);
        RendimentoSimulacaoDTO rendimentoSimulacaoDTO = new RendimentoSimulacaoDTO();


        try {
            rendimentoSimulacaoDTO = simulacaoService.salvarSimulacao(simulacaoDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        return rendimentoSimulacaoDTO;
    }

}
