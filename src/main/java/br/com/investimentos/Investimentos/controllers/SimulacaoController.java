package br.com.investimentos.Investimentos.controllers;

import br.com.investimentos.Investimentos.DTOs.SimulacaoDTO;
import br.com.investimentos.Investimentos.models.Simulacao;
import br.com.investimentos.Investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<SimulacaoDTO> lerTodasAsSimulacoes(){
        return simulacaoService.lerTodasSimulacoes();
    }
}
