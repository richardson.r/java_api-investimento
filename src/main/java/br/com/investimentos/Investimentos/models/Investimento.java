package br.com.investimentos.Investimentos.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import java.math.BigDecimal;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
public class Investimento {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int id;

    @NotNull
    @NotBlank
    @Column(unique = true)
    private String nome;

    @NotNull
    private BigDecimal rendimentoAoMes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(BigDecimal rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public Investimento() {
    }

}
