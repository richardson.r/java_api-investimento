package br.com.investimentos.Investimentos.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import java.math.BigDecimal;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = IDENTITY) //Gera id da coluna primária
    private int id;

    @NotNull
    @ManyToOne
    private Investimento investimento;

    @NotNull
    @Digits(integer = 12, fraction = 2, message = "Valor aplicado fora do padrão")
    @DecimalMin(value = "0.01", message = "O valor aplicado não pode ser inferior a 0.01")
    private BigDecimal valorAplicado;

    @NotNull
    @DecimalMin(value = "1", message = "A quantidade mínima de meses é 1")
    private int quantidadeMeses;

    @NotBlank
    @NotNull
    private String nomeUsuario;

    @NotBlank
    @NotNull
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }

    public BigDecimal getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(BigDecimal valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
